﻿using System;
using System.Collections.Generic;

namespace Project.Application.ViewModels
{
    public class CandidatoViewModel
    {
        public Guid Id { get; set; }
        public string Cep { get; set; }
        public string Cpf { get; set; }
        public DateTime DataDeNascimento { get; set; }
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public virtual ICollection<InscricaoViewModel> InscricoesCollection { get; set; }
    }
}