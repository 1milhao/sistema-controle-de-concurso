﻿using System;
using System.Collections.Generic;

namespace Project.Application.ViewModels
{
    public class ConcursoViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public DateTime DataInicioInscricao { get; set; }
        public DateTime DataFimInscricao { get; set; }
        public DateTime DataLimiteParaPagamento { get; set; }
        public virtual InstituicaoViewModel Instituicao { get; set; }
        public virtual ComissaoViewModel Comissao { get; set; }
        public ICollection<AtosDaComissaoViewModel> AtosDaComissaoCollections { get; set; }
        public ICollection<VagasViewModel> VagasCollections { get; set; }
    }
}