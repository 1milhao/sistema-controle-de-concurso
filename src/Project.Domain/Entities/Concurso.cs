﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class Concurso
    {
        public Concurso(Guid id, string nome, string status, DateTime dataInicioInscricao, DateTime dataFimInscricao, DateTime dataLimiteParaPagamento, Guid instituicaoId, Guid comissaoId)
        {
            Id = id;
            Nome = nome;
            Status = status;
            DataInicioInscricao = dataInicioInscricao;
            DataFimInscricao = dataFimInscricao;
            DataLimiteParaPagamento = dataLimiteParaPagamento;
            InstituicaoId = instituicaoId;
            ComissaoId = comissaoId;
        }

        protected Concurso()
        {
            
        }

        public Guid Id { get; private set; }
        public Guid InstituicaoId { get; private set; }
        public Guid ComissaoId { get; private set; }
        public string Nome { get; private set; }
        public string Status { get; private set; }
        public DateTime DataInicioInscricao { get; private set; }
        public DateTime DataFimInscricao { get; private set; }
        public DateTime DataLimiteParaPagamento { get; private set; }
        public virtual Instituicao Instituicao { get; set; }
        public virtual Comissao Comissao { get; set; }
        public ICollection<AtosDaComissao> AtosDaComissaoCollections { get; set; }  
        public ICollection<Vagas> VagasCollections { get; set; }  
    }
}