﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class ComissaoAppService : IComissaoAppService
    {
        private readonly IMapper _mapper;
        private readonly IComissaoService _service;

        public ComissaoAppService(IMapper mapper, IComissaoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<ComissaoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<ComissaoViewModel>>(_service.ObterTodos());
        }

        public ComissaoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<ComissaoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(ComissaoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Comissao>(obj));
        }

        public void Atualizar(ComissaoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Comissao>(obj));
        }

        public void Remover(ComissaoViewModel obj)
        {
            _service.Remover(_mapper.Map<Comissao>(obj));
        }
    }
}