﻿using System.ComponentModel;
using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class InscricaoConfig : EntityTypeConfiguration<Inscricao>
    {
        public InscricaoConfig()
        {
            //PK
            HasKey(i => i.Id);

            //PROPERTIES
            Property(i => i.Status)
                .HasMaxLength(100)
                .IsRequired();

            Property(i => i.TipoInscricao)
                .HasMaxLength(100)
                .IsRequired();

            //DATA
            ToTable("Inscricao");
            Property(i => i.Id).HasColumnName("Id");
            Property(i => i.Ativa).HasColumnName("Ativa");
            Property(i => i.DataCadastro).HasColumnName("DataCadastro");
            Property(i => i.Status).HasColumnName("Status");
            Property(i => i.Tipo).HasColumnName("Tipo");
            Property(i => i.TipoInscricao).HasColumnName("TipoInscricao");

            //RELATIONS
            HasRequired(i => i.Vagas)
                .WithMany(v => v.InscricoesCollection)
                .HasForeignKey(i => i.VagasId);
        }
    }
}