﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class InstituicaoController : Controller
    {
        private readonly IInstituicaoAppService _instituicaoAppService;

        public InstituicaoController(IInstituicaoAppService instituicaoAppService)
        {
            _instituicaoAppService = instituicaoAppService;
        }

        public ActionResult Index()
        {
            return View(_instituicaoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var instituicaoViewModel = _instituicaoAppService.ObterPorId(id);
            if (instituicaoViewModel != null) return View(instituicaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(InstituicaoViewModel instituicaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _instituicaoAppService.Adicionar(instituicaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(instituicaoViewModel);
            }
            catch
            {
                return View(instituicaoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var instituicaoViewModel = _instituicaoAppService.ObterPorId(id);
            if (instituicaoViewModel != null) return View(instituicaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(InstituicaoViewModel instituicaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _instituicaoAppService.Atualizar(instituicaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(instituicaoViewModel);
            }
            catch
            {
                return View(instituicaoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var instituicaoViewModel = _instituicaoAppService.ObterPorId(id);
            if (instituicaoViewModel != null) return View(instituicaoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(InstituicaoViewModel instituicaoViewModel)
        {
            try
            {
                _instituicaoAppService.Remover(instituicaoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(instituicaoViewModel);
            }
        }
    }
}