﻿using Project.Domain.Entities;

namespace Project.Domain.Interfaces.Repository
{
    public interface ICargoRepository : IRepositoryBase<Cargo>
    {
        
    }
}