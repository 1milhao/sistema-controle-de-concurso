﻿using System;
using System.Collections.Generic;
using Project.Domain.Entities.Enum;

namespace Project.Application.ViewModels
{
    public class VagasViewModel
    {
        public Guid Id { get; set; }
        public string EscolaridadeExigida { get; set; }
        public int QuantidadeDeVagas { get; set; }
        public string TipoDeVaga { get; set; }
        public decimal ValorInscricao { get; set; }
        public Tipo Tipo { get; set; }
        public virtual ConcursoViewModel Concurso { get; set; }
        public virtual ICollection<InscricaoViewModel> InscricoesCollection { get; set; }
    }
}