﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IComissaoAppService
    {
        IEnumerable<ComissaoViewModel> ObterTodos();
        ComissaoViewModel ObterPorId(Guid id);
        void Adicionar(ComissaoViewModel obj);
        void Atualizar(ComissaoViewModel obj);
        void Remover(ComissaoViewModel obj);
    }
}