﻿using Project.Domain.Entities;

namespace Project.Domain.Interfaces.Repository
{
    public interface IComissaoRepository : IRepositoryBase<Comissao>
    {
        
    }
}