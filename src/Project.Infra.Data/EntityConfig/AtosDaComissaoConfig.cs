﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class AtosDaComissaoConfig : EntityTypeConfiguration<AtosDaComissao>
    {
        public AtosDaComissaoConfig()
        {
            //PK
            HasKey(ac => ac.Id);

            //PROPERTIES
            
            //DATA
            ToTable("AtosDaComissao");
            Property(ac => ac.Id).HasColumnName("Id");
            Property(ac => ac.DataInicioDaVisibilidade).HasColumnName("DataInicioDaVisibilidade");
            Property(ac => ac.DataFimDaVisibilidade).HasColumnName("DataFimDaVisibilidade");
            Property(ac => ac.Tipo).HasColumnName("Tipo");

            //RELATIONS
            HasRequired(ac => ac.Concurso)
                .WithMany(c => c.AtosDaComissaoCollections)
                .HasForeignKey(ac => ac.ConcursoId);
        }
    }
}