﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IFuncionarioAppService
    {
        IEnumerable<FuncionarioViewModel> ObterTodos();
        FuncionarioViewModel ObterPorId(Guid id);
        void Adicionar(FuncionarioViewModel obj);
        void Atualizar(FuncionarioViewModel obj);
        void Remover(FuncionarioViewModel obj);
    }
}