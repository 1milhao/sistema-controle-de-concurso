﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class Cargo
    {
        public Cargo(Guid id, string descricao)
        {
            Id = id;
            Descricao = descricao;
        }

        protected Cargo()
        {
            
        }
        public Guid Id { get; private set; }
        public string Descricao { get; private set; }
        public virtual ICollection<Vagas> VagasCollection { get; set; }
    }
}