﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class ComissaoController : Controller
    {
        private readonly IComissaoAppService _comissaoAppService;

        public ComissaoController(IComissaoAppService comissaoAppService)
        {
            _comissaoAppService = comissaoAppService;
        }

        public ActionResult Index()
        {
            return View(_comissaoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var comissaoViewModel = _comissaoAppService.ObterPorId(id);
            if (comissaoViewModel != null) return View(comissaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ComissaoViewModel comissaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _comissaoAppService.Adicionar(comissaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(comissaoViewModel);
            }
            catch
            {
                return View(comissaoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var comissaoViewModel = _comissaoAppService.ObterPorId(id);
            if (comissaoViewModel != null) return View(comissaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(ComissaoViewModel comissaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _comissaoAppService.Atualizar(comissaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(comissaoViewModel);
            }
            catch
            {
                return View(comissaoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var comissaoViewModel = _comissaoAppService.ObterPorId(id);
            if (comissaoViewModel != null) return View(comissaoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(ComissaoViewModel comissaoViewModel)
        {
            try
            {
                _comissaoAppService.Remover(comissaoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(comissaoViewModel);
            }
        }
    }
}