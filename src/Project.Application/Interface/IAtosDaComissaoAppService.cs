﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IAtosDaComissaoAppService
    {
        IEnumerable<AtosDaComissaoViewModel> ObterTodos();
        AtosDaComissaoViewModel ObterPorId(Guid id);
        void Adicionar(AtosDaComissaoViewModel obj);
        void Atualizar(AtosDaComissaoViewModel obj);
        void Remover(AtosDaComissaoViewModel obj);
    }
}