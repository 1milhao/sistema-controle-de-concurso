﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class BoletoConfig : EntityTypeConfiguration<Boleto>
    {
        public BoletoConfig()
        {
            //PK
            HasKey(b => b.Id);

            //PROPERTIES

            //DATA
            ToTable("Boleto");
            Property(b => b.Id).HasColumnName("Id");
            Property(b => b.Arquivo).HasColumnName("Arquivo");
            Property(b => b.Ativo).HasColumnName("Ativo");
            Property(b => b.DataDeValidade).HasColumnName("DataDeValidade");

        }
    }
}