﻿using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ProjectContext Context;
        private bool _disposed;

        public UnitOfWork(ProjectContext context)
        {
            Context = context;
        }

        public void BeginTransaction()
        {
            _disposed = false;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}