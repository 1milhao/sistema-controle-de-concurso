﻿using System;

namespace Project.Application.ViewModels
{
    public class FuncionarioViewModel
    {
        public Guid Id { get; set; }
        public virtual ComissaoViewModel Comissao { get; set; }
    }
}