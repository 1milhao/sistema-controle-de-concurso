﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class Instituicao
    {
        public Instituicao(Guid id)
        {
            Id = id;
        }

        protected Instituicao()
        {
            
        }

        public Guid Id { get; private set; }
        public ICollection<Concurso> ConcursosCollections { get; set; }
    }
}