﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class CandidatoController : Controller
    {
        private readonly ICandidatoAppService _candidatoAppService;

        public CandidatoController(ICandidatoAppService candidatoAppService)
        {
            _candidatoAppService = candidatoAppService;
        }

        public ActionResult Index()
        {
            return View(_candidatoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var candidatoViewModel = _candidatoAppService.ObterPorId(id);
            if (candidatoViewModel != null) return View(candidatoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CandidatoViewModel candidatoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _candidatoAppService.Adicionar(candidatoViewModel);
                    return RedirectToAction("Index");
                }
                return View(candidatoViewModel);
            }
            catch
            {
                return View(candidatoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var candidatoViewModel = _candidatoAppService.ObterPorId(id);
            if (candidatoViewModel != null) return View(candidatoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(CandidatoViewModel candidatoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _candidatoAppService.Adicionar(candidatoViewModel);
                    return RedirectToAction("Index");
                }
                return View(candidatoViewModel);
            }
            catch
            {
                return View(candidatoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var candidatoViewModel = _candidatoAppService.ObterPorId(id);
            if (candidatoViewModel != null) return View(candidatoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(CandidatoViewModel candidatoViewModel)
        {
            try
            {
                _candidatoAppService.Remover(candidatoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(candidatoViewModel);
            }
        }
    }
}
