﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.Repositories
{
    public class CargoRepository : RepositoryBase<Cargo>, ICargoRepository
    {
        public CargoRepository(ProjectContext context, IUnitOfWork uow) : base(context, uow)
        {
        }
    }
}