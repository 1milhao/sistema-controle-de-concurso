﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class CandidatoConfig : EntityTypeConfiguration<Candidato>
    {
        public CandidatoConfig()
        {
            //PK
            HasKey(c => c.Id);

            //PROPERTIES

            Property(c => c.Nome)
                .HasMaxLength(200)
                .IsRequired();

            Property(c => c.Sexo)
                .HasMaxLength(50);

            //DATA
            ToTable("Candidato");
            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Cep).HasColumnName("Cep");
            Property(c => c.Cpf).HasColumnName("Cpf");
            Property(c => c.DataDeNascimento).HasColumnName("DataDeNascimento");
            Property(c => c.Nome).HasColumnName("Nome");
            Property(c => c.Sexo).HasColumnName("Sexo");

        }
    }
}