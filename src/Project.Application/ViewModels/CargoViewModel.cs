﻿using System;
using System.Collections.Generic;

namespace Project.Application.ViewModels
{
    public class CargoViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<VagasViewModel> VagasCollection { get; set; }
    }
}