﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class Comissao
    {
        public Comissao(Guid id)
        {
            Id = id;
        }

        protected Comissao()
        {
            
        }

        public Guid Id { get; private set; }
        public ICollection<Concurso> ConcursosCollections { get; set; }
        public ICollection<Funcionario> FuncionariosCollections { get; set; }
    }
}