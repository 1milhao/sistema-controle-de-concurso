﻿using System;
using Project.Domain.Entities.Enum;

namespace Project.Domain.Entities
{
    public class Inscricao
    {
        public Inscricao(Guid id, bool ativa, DateTime dataCadastro, string status, string tipoInscricao, Tipo tipo, Guid vagasId, Guid boletoId)
        {
            Id = id;
            Ativa = ativa;
            DataCadastro = dataCadastro;
            Status = status;
            TipoInscricao = tipoInscricao;
            Tipo = tipo;
            VagasId = vagasId;
            BoletoId = boletoId;
        }

        protected Inscricao()
        {
            
        }

        public Guid Id { get; private set; }
        public Guid VagasId { get; private set; }
        public Guid BoletoId { get; private set; }
        public bool Ativa { get; private set; }
        public DateTime DataCadastro { get; private set; }
        public string Status { get; private set; }
        public string TipoInscricao { get; private set; }
        public Tipo Tipo { get; private set; }
        public virtual Vagas Vagas { get; set; }
        public virtual Boleto Boleto { get; set; }
    }
}