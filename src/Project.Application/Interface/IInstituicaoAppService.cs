﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IInstituicaoAppService
    {
        IEnumerable<InstituicaoViewModel> ObterTodos();
        InstituicaoViewModel ObterPorId(Guid id);
        void Adicionar(InstituicaoViewModel obj);
        void Atualizar(InstituicaoViewModel obj);
        void Remover(InstituicaoViewModel obj);
    }
}