﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class VagasConfig : EntityTypeConfiguration<Vagas>
    {
        public VagasConfig()
        {
            //PK
            HasKey(v => v.Id);

            //PROPERTIES

            //DATA
            ToTable("Vagas");
            Property(v => v.Id).HasColumnName("Id");
            Property(v => v.EscolaridadeExigida).HasColumnName("EscolaridadeExigida");
            Property(v => v.QuantidadeDeVagas).HasColumnName("QuantidadeDeVagas");
            Property(v => v.Tipo).HasColumnName("Tipo");
            Property(v => v.TipoDeVaga).HasColumnName("TipoDeVaga");
            Property(v => v.ValorInscricao).HasColumnName("ValorInscricao");

            //RELATIONS
            HasRequired(v => v.Concurso)
                .WithMany(c => c.VagasCollections)
                .HasForeignKey(v => v.ConcursoId);

        }
    }
}