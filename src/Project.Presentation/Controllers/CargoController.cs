﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class CargoController : Controller
    {
        private readonly ICargoAppService _cargoAppService;

        public CargoController(ICargoAppService cargoAppService)
        {
            _cargoAppService = cargoAppService;
        }

        public ActionResult Index()
        {
            return View(_cargoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var cargoViewModel = _cargoAppService.ObterPorId(id);
            if (cargoViewModel != null) return View(cargoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CargoViewModel cargoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cargoAppService.Adicionar(cargoViewModel);
                    return RedirectToAction("Index");
                }
                return View(cargoViewModel);
            }
            catch
            {
                return View(cargoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var cargoViewModel = _cargoAppService.ObterPorId(id);
            if (cargoViewModel != null) return View(cargoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(CargoViewModel cargoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _cargoAppService.Atualizar(cargoViewModel);
                    return RedirectToAction("Index");
                }
                return View(cargoViewModel);
            }
            catch
            {
                return View(cargoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var cargoViewModel = _cargoAppService.ObterPorId(id);
            if (cargoViewModel != null) return View(cargoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(CargoViewModel cargoViewModel)
        {
            try
            {
                _cargoAppService.Remover(cargoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(cargoViewModel);
            }
        }
    }
}