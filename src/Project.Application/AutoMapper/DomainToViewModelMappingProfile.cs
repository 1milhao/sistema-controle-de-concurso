﻿using AutoMapper;
using Project.Application.ViewModels;
using Project.Domain.Entities;

namespace Project.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<AtosDaComissao, AtosDaComissaoViewModel>();
            CreateMap<Boleto, BoletoViewModel>();
            CreateMap<Candidato, CandidatoViewModel>();
            CreateMap<Cargo, CargoViewModel>();
            CreateMap<Comissao, ComissaoViewModel>();
            CreateMap<Concurso, ConcursoViewModel>();
            CreateMap<Funcionario, FuncionarioViewModel>();
            CreateMap<Inscricao, InscricaoViewModel>();
            CreateMap<Instituicao, InstituicaoViewModel>();
            CreateMap<Vagas, VagasViewModel>();
        }
    }
}