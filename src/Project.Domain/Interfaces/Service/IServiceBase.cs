﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Project.Domain.Interfaces.Service
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> ObterTodos();
        TEntity ObterPorId(Guid id);
        void Adicionar(TEntity obj);
        void Atualizar(TEntity obj);
        void Remover(TEntity obj);
        void Dispose();
    }
}