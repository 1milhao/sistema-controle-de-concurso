﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class FuncionarioConfig : EntityTypeConfiguration<Funcionario>
    {
        public FuncionarioConfig()
        {
            //PK
            HasKey(f => f.Id);

            //PROPERTIES

            //DATA
            ToTable("Funcionario");
            Property(f => f.Id).HasColumnName("Id");

            //RELATIONS
            HasRequired(f => f.Comissao)
                .WithMany(c => c.FuncionariosCollections)
                .HasForeignKey(f => f.ComissaoId);
        }
    }
}