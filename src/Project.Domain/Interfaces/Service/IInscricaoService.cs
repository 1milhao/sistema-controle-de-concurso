﻿using Project.Domain.Entities;

namespace Project.Domain.Interfaces.Service
{
    public interface IInscricaoService : IServiceBase<Inscricao>
    {
        
    }
}