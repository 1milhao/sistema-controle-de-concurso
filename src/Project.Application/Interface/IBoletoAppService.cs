﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IBoletoAppService
    {
        IEnumerable<BoletoViewModel> ObterTodos();
        BoletoViewModel ObterPorId(Guid id);
        void Adicionar(BoletoViewModel obj);
        void Atualizar(BoletoViewModel obj);
        void Remover(BoletoViewModel obj);
    }
}