﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class AtosDaComissao
    {
        public AtosDaComissao(Guid id, int dataFimDaVisibilidade, DateTime dataInicioDaVisibilidade, string tipo, bool ativo, byte[] arquivo, Guid concursoId)
        {
            Id = id;
            DataFimDaVisibilidade = dataFimDaVisibilidade;
            DataInicioDaVisibilidade = dataInicioDaVisibilidade;
            Tipo = tipo;
            Ativo = ativo;
            Arquivo = arquivo;
            ConcursoId = concursoId;
        }

        protected AtosDaComissao()
        {
            
        }

        public Guid Id { get; private set; }
        public Guid ConcursoId { get; private set; }
        public int DataFimDaVisibilidade { get; private set; }
        public DateTime DataInicioDaVisibilidade { get; private set; }
        public string Tipo { get; private set; }
        public byte[] Arquivo { get; private set; }
        public bool Ativo { get; private set; }
        public virtual Concurso Concurso { get; set; }
    }
}