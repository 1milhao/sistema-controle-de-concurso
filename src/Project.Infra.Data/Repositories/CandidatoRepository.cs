﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.Repositories
{
    public class CandidatoRepository : RepositoryBase<Candidato>, ICandidatoRepository
    {
        public CandidatoRepository(ProjectContext context, IUnitOfWork uow) : base(context, uow)
        {
        }
    }
}