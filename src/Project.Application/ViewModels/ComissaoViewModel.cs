﻿using System;
using System.Collections.Generic;

namespace Project.Application.ViewModels
{
    public class ComissaoViewModel
    {
        public Guid Id { get; set; }
        public ICollection<ConcursoViewModel> ConcursosCollections { get; set; }
        public ICollection<FuncionarioViewModel> FuncionariosCollections { get; set; }
    }
}