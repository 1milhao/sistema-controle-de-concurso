﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class AtosDaComissaoService : ServiceBase<AtosDaComissao>, IAtosDaComissaoService
    {
        private readonly IAtosDaComissaoRepository _repository;
        public AtosDaComissaoService(IAtosDaComissaoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}