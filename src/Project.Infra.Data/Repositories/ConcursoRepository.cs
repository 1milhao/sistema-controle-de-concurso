﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.Repositories
{
    public class ConcursoRepository : RepositoryBase<Concurso>, IConcursoRepository
    {
        public ConcursoRepository(ProjectContext context, IUnitOfWork uow) : base(context, uow)
        {
        }
    }
}