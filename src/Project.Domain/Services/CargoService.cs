﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class CargoService : ServiceBase<Cargo>, ICargoService
    {
        private readonly ICargoRepository _repository;
        public CargoService(ICargoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}