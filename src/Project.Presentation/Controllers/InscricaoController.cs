﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class InscricaoController : Controller
    {
        private readonly IInscricaoAppService _inscricaoAppService;

        public InscricaoController(IInscricaoAppService inscricaoAppService)
        {
            _inscricaoAppService = inscricaoAppService;
        }

        public ActionResult Index()
        {
            return View(_inscricaoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var inscricaoViewModel = _inscricaoAppService.ObterPorId(id);
            if (inscricaoViewModel != null) return View(inscricaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(InscricaoViewModel inscricaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _inscricaoAppService.Adicionar(inscricaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(inscricaoViewModel);
            }
            catch
            {
                return View(inscricaoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var inscricaoViewModel = _inscricaoAppService.ObterPorId(id);
            if (inscricaoViewModel != null) return View(inscricaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(InscricaoViewModel inscricaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _inscricaoAppService.Adicionar(inscricaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(inscricaoViewModel);
            }
            catch
            {
                return View(inscricaoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var inscricaoViewModel = _inscricaoAppService.ObterPorId(id);
            if (inscricaoViewModel != null) return View(inscricaoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(InscricaoViewModel inscricaoViewModel)
        {
            try
            {
                _inscricaoAppService.Remover(inscricaoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(inscricaoViewModel);
            }
        }
    }
}