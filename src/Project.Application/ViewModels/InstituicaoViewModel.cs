﻿using System;
using System.Collections.Generic;

namespace Project.Application.ViewModels
{
    public class InstituicaoViewModel
    {
        public Guid Id { get; set; }
        public ICollection<ConcursoViewModel> ConcursosCollections { get; set; }
    }
}