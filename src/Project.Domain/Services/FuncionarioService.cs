﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class FuncionarioService : ServiceBase<Funcionario>, IFuncionarioService
    {
        private readonly IFuncionarioRepository _repository;
        public FuncionarioService(IFuncionarioRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}