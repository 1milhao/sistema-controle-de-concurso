﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class InstituicaoConfig : EntityTypeConfiguration<Instituicao>
    {
        public InstituicaoConfig()
        {
            //PK
            HasKey(i => i.Id);

            //PROPERTIES

            //DATA
            ToTable("Instituicao");
            Property(i => i.Id).HasColumnName("Id");
        }
    }
}