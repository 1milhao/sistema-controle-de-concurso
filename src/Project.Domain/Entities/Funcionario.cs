﻿using System;

namespace Project.Domain.Entities
{
    public class Funcionario : Pessoa
    {
        public Funcionario(Guid id, Guid comissaoId)
        {
            Id = id;
            ComissaoId = comissaoId;
        }

        protected Funcionario()
        {
            
        }

        public Guid Id { get; private set; }
        public Guid ComissaoId { get; private set; }
        public virtual Comissao Comissao { get; set; }
    }
}