﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class InstituicaoService : ServiceBase<Instituicao>, IInstituicaoService
    {
        private readonly IInstituicaoRepository _repository;
        public InstituicaoService(IInstituicaoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}