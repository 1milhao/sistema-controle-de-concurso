﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class ConcursoAppService : IConcursoAppService
    {
        private readonly IMapper _mapper;
        private readonly IConcursoService _service;

        public ConcursoAppService(IMapper mapper, IConcursoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<ConcursoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<ConcursoViewModel>>(_service.ObterTodos());
        }

        public ConcursoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<ConcursoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(ConcursoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Concurso>(obj));
        }

        public void Atualizar(ConcursoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Concurso>(obj));
        }

        public void Remover(ConcursoViewModel obj)
        {
            _service.Remover(_mapper.Map<Concurso>(obj));
        }

    }
}