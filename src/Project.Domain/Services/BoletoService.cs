﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class BoletoService : ServiceBase<Boleto>, IBoletoService
    {
        private readonly IBoletoRepository _repository;
        public BoletoService(IBoletoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}