﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class CargoAppService : ICargoAppService
    {
        private readonly IMapper _mapper;
        private readonly ICargoService _service;

        public CargoAppService(ICargoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<CargoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<CargoViewModel>>(_service.ObterTodos());
        }

        public CargoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<CargoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(CargoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Cargo>(obj));
        }

        public void Atualizar(CargoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Cargo>(obj));
        }

        public void Remover(CargoViewModel obj)
        {
            _service.Remover(_mapper.Map<Cargo>(obj));
        }
    }
}