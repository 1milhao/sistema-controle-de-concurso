﻿using System;
using System.Collections.Generic;
using Project.Domain.Entities.Enum;

namespace Project.Domain.Entities
{
    public class Vagas
    {
        public Vagas(Guid id, string escolaridadeExigida, int quantidadeDeVagas, string tipoDeVaga, decimal valorInscricao, Tipo tipo, Guid concursoId)
        {
            Id = id;
            EscolaridadeExigida = escolaridadeExigida;
            QuantidadeDeVagas = quantidadeDeVagas;
            TipoDeVaga = tipoDeVaga;
            ValorInscricao = valorInscricao;
            Tipo = tipo;
            ConcursoId = concursoId;
        }

        protected Vagas()
        {
            
        }

        public Guid Id { get; private set; }
        public Guid ConcursoId { get; private set; }
        public string EscolaridadeExigida { get; private set; }
        public int QuantidadeDeVagas{ get; private set; }
        public string TipoDeVaga{ get; private set; }
        public decimal ValorInscricao{ get; private set; }
        public Tipo Tipo { get; private set; }
        public virtual Concurso Concurso { get; set; }
        public virtual  ICollection<Inscricao> InscricoesCollection { get; set; }
    }
}