﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class CargoConfig : EntityTypeConfiguration<Cargo>
    {
        public CargoConfig()
        {
            //PK
            HasKey(c => c.Id);

            //PROPERTIES
            Property(c => c.Descricao)
                .HasMaxLength(250)
                .IsRequired();

            //DATA
            ToTable("Cargo");
            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.Descricao).HasColumnName("Descricao");
        }
    }
}