﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IInscricaoAppService
    {
        IEnumerable<InscricaoViewModel> ObterTodos();
        InscricaoViewModel ObterPorId(Guid id);
        void Adicionar(InscricaoViewModel obj);
        void Atualizar(InscricaoViewModel obj);
        void Remover(InscricaoViewModel obj);
    }
}