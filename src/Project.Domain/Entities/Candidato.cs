﻿using System;
using System.Collections.Generic;

namespace Project.Domain.Entities
{
    public class Candidato : Pessoa
    {
        public Candidato(Guid id, string cep, string cpf, DateTime dataDeNascimento, string nome, string sexo)
        {
            Id = id;
            Cep = cep;
            Cpf = cpf;
            DataDeNascimento = dataDeNascimento;
            Nome = nome;
            Sexo = sexo;
        }

        protected Candidato()
        {
            
        }
        public Guid Id { get; private set; }
        public string Cep { get; private set; }
        public string Cpf { get; private set; }
        public DateTime DataDeNascimento { get; private set; }
        public string Nome { get; private set; }
        public string Sexo { get; private set; }
        public virtual ICollection<Inscricao> InscricoesCollection { get; set; }
    }
}