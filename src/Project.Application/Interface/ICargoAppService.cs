﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface ICargoAppService
    {
        IEnumerable<CargoViewModel> ObterTodos();
        CargoViewModel ObterPorId(Guid id);
        void Adicionar(CargoViewModel obj);
        void Atualizar(CargoViewModel obj);
        void Remover(CargoViewModel obj);
    }
}