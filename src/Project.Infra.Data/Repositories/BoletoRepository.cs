﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.Repositories
{
    public class BoletoRepository : RepositoryBase<Boleto>, IBoletoRepository
    {
        public BoletoRepository(ProjectContext context, IUnitOfWork uow) : base(context, uow)
        {
        }
    }
}