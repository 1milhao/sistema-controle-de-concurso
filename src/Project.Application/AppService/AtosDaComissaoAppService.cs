﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class AtosDaComissaoAppService : IAtosDaComissaoAppService
    {
        private readonly IAtosDaComissaoService _service;
        private readonly IMapper _mapper;

        public AtosDaComissaoAppService(IAtosDaComissaoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<AtosDaComissaoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<AtosDaComissaoViewModel>>(_service.ObterTodos());
        }

        public AtosDaComissaoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<AtosDaComissaoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(AtosDaComissaoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<AtosDaComissao>(obj));
        }

        public void Atualizar(AtosDaComissaoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<AtosDaComissao>(obj));
        }

        public void Remover(AtosDaComissaoViewModel obj)
        {
            _service.Remover(_mapper.Map<AtosDaComissao>(obj));
        }
    }
}