﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;
using Project.Domain.Interfaces.Service;

namespace Project.Application.Interface
{
    public interface ICandidatoAppService
    {
        IEnumerable<CandidatoViewModel> ObterTodos();
        CandidatoViewModel ObterPorId(Guid id);
        void Adicionar(CandidatoViewModel obj);
        void Atualizar(CandidatoViewModel obj);
        void Remover(CandidatoViewModel obj);
    }
}