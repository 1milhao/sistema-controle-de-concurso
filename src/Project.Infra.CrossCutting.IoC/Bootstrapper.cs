﻿using AutoMapper;
using Project.Application.AppService;
using Project.Application.AutoMapper;
using Project.Application.Interface;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;
using Project.Domain.Services;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;
using Project.Infra.Data.Repositories;
using Project.Infra.Data.UoW;
using SimpleInjector;

namespace Project.Infra.CrossCutting.IoC
{
    public class Bootstrapper
    {
        public static Container MyContainer { get; set; }

        public static void RegisterServices(Container container)
        {
            //AutoMapper
            var config = AutoMapperConfig.RegisterMappings();

            container.RegisterSingleton(config);
            container.Register(() => container.GetInstance<MapperConfiguration>().CreateMapper(), Lifestyle.Scoped);

            //Infra Dados
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<ProjectContext>(Lifestyle.Scoped);

            // APP
            container.Register<IAtosDaComissaoAppService, AtosDaComissaoAppService>(Lifestyle.Scoped);
            container.Register<IBoletoAppService, BoletoAppService>(Lifestyle.Scoped);
            container.Register<ICandidatoAppService, CandidatoAppService>(Lifestyle.Scoped);
            container.Register<ICargoAppService, CargoAppService>(Lifestyle.Scoped);
            container.Register<IComissaoAppService, ComissaoAppService>(Lifestyle.Scoped);
            container.Register<IConcursoAppService, ConcursoAppService>(Lifestyle.Scoped);
            container.Register<IFuncionarioAppService, FuncionarioAppService>(Lifestyle.Scoped);
            container.Register<IInscricaoAppService, InscricaoAppService>(Lifestyle.Scoped);
            container.Register<IInstituicaoAppService, InstituicaoAppService>(Lifestyle.Scoped);
            container.Register<IVagasAppService, VagasAppService>(Lifestyle.Scoped);

            // Domain
            container.Register<IAtosDaComissaoService, AtosDaComissaoService>(Lifestyle.Scoped);
            container.Register<IBoletoService, BoletoService>(Lifestyle.Scoped);
            container.Register<ICandidatoService, CandidatoService>(Lifestyle.Scoped);
            container.Register<ICargoService, CargoService>(Lifestyle.Scoped);
            container.Register<IComissaoService, ComissaoService>(Lifestyle.Scoped);
            container.Register<IConcursoService, ConcursoService>(Lifestyle.Scoped);
            container.Register<IFuncionarioService, FuncionarioService>(Lifestyle.Scoped);
            container.Register<IInscricaoService, InscricaoService>(Lifestyle.Scoped);
            container.Register<IInstituicaoService, InstituicaoService>(Lifestyle.Scoped);
            container.Register<IVagasService, VagasService>(Lifestyle.Scoped);

            // Infra Dados Repos
            container.Register<IAtosDaComissaoRepository, AtosDaComissaoRepository>(Lifestyle.Scoped);
            container.Register<IBoletoRepository, BoletoRepository>(Lifestyle.Scoped);
            container.Register<ICandidatoRepository, CandidatoRepository>(Lifestyle.Scoped);
            container.Register<ICargoRepository, CargoRepository>(Lifestyle.Scoped);
            container.Register<IComissaoRepository, ComissaoRepository>(Lifestyle.Scoped);
            container.Register<IConcursoRepository, ConcursoRepository>(Lifestyle.Scoped);
            container.Register<IFuncionarioRepository, FuncionarioRepository>(Lifestyle.Scoped);
            container.Register<IInscricaoRepository, InscricaoRepository>(Lifestyle.Scoped);
            container.Register<IInstituicaoRepository, InstituicaoRepository>(Lifestyle.Scoped);
            container.Register<IVagasRepository, VagasRepository>(Lifestyle.Scoped);
        }
    }
}