﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class ConcursoConfig : EntityTypeConfiguration<Concurso>
    {
        public ConcursoConfig()
        {
            //PK
            HasKey(c => c.Id);

            //PROPERTIES
            Property(c => c.Nome)
                .HasMaxLength(200)
                .IsRequired();

            Property(c => c.Status)
                .HasMaxLength(50)
                .IsRequired();

            //DATA
            ToTable("Concurso");
            Property(c => c.Id).HasColumnName("Id");
            Property(c => c.DataInicioInscricao).HasColumnName("DataInicioInscricao");
            Property(c => c.DataFimInscricao).HasColumnName("DataFimInscricao");
            Property(c => c.DataLimiteParaPagamento).HasColumnName("DataLimiteParaPagamento");
            Property(c => c.Nome).HasColumnName("Nome");
            Property(c => c.Status).HasColumnName("Status");

            //RELATIONS
            HasRequired(c => c.Comissao)
                .WithMany(co => co.ConcursosCollections)
                .HasForeignKey(c => c.ComissaoId);

            HasRequired(c => c.Instituicao)
                .WithMany(co => co.ConcursosCollections)
                .HasForeignKey(c => c.InstituicaoId);
        }
    }
}