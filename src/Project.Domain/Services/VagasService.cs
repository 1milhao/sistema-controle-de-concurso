﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class VagasService : ServiceBase<Vagas>, IVagasService
    {
        private readonly IVagasRepository _repository;
        public VagasService(IVagasRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}