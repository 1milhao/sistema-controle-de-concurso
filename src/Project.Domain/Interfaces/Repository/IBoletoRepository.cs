﻿using Project.Domain.Entities;

namespace Project.Domain.Interfaces.Repository
{
    public interface IBoletoRepository : IRepositoryBase<Boleto>
    {
        
    }
}