﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class InstituicaoAppService : IInstituicaoAppService
    {
        private readonly IMapper _mapper;
        private readonly IInstituicaoService _service;

        public InstituicaoAppService(IMapper mapper, IInstituicaoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<InstituicaoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<InstituicaoViewModel>>(_service.ObterTodos());
        }

        public InstituicaoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<InstituicaoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(InstituicaoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Instituicao>(obj));
        }

        public void Atualizar(InstituicaoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Instituicao>(obj));
        }

        public void Remover(InstituicaoViewModel obj)
        {
            _service.Remover(_mapper.Map<Instituicao>(obj));
        }
    }
}