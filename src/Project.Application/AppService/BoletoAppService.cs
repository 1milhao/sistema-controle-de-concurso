﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class BoletoAppService : IBoletoAppService
    {
        private readonly IBoletoService _service;
        private readonly IMapper _mapper;

        public BoletoAppService(IBoletoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public IEnumerable<BoletoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<BoletoViewModel>>(_service.ObterTodos());
        }

        public BoletoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<BoletoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(BoletoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Boleto>(obj));
        }

        public void Atualizar(BoletoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Boleto>(obj));
        }

        public void Remover(BoletoViewModel obj)
        {
            _service.Remover(_mapper.Map<Boleto>(obj));
        }
    }
}