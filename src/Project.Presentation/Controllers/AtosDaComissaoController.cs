﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class AtosDaComissaoController : Controller
    {
        private readonly IAtosDaComissaoAppService _atosDaComissaoAppService;

        public AtosDaComissaoController(IAtosDaComissaoAppService atosDaComissaoAppService)
        {
            _atosDaComissaoAppService = atosDaComissaoAppService;
        }

        public ActionResult Index()
        {
            return View(_atosDaComissaoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var atosDaComissaoViewModel = _atosDaComissaoAppService.ObterPorId(id);
            if (atosDaComissaoViewModel != null) return View(atosDaComissaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AtosDaComissaoViewModel atosDaComissaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _atosDaComissaoAppService.Adicionar(atosDaComissaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(atosDaComissaoViewModel);
            }
            catch
            {
                return View(atosDaComissaoViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var atosDaComissaoViewModel = _atosDaComissaoAppService.ObterPorId(id);
            if (atosDaComissaoViewModel != null) return View(atosDaComissaoViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(AtosDaComissaoViewModel atosDaComissaoViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _atosDaComissaoAppService.Atualizar(atosDaComissaoViewModel);
                    return RedirectToAction("Index");
                }
                return View(atosDaComissaoViewModel);
            }
            catch
            {
                return View(atosDaComissaoViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var atosDaComissaoViewModel = _atosDaComissaoAppService.ObterPorId(id);
            if (atosDaComissaoViewModel != null) return View(atosDaComissaoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(AtosDaComissaoViewModel atosDaComissaoViewModel)
        {
            try
            {
                _atosDaComissaoAppService.Remover(atosDaComissaoViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(atosDaComissaoViewModel);
            }
        }
    }
}