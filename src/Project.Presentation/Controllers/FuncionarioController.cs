﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class FuncionarioController : Controller
    {
        private readonly IFuncionarioAppService _funcionarioAppService;

        public FuncionarioController(IFuncionarioAppService funcionarioAppService)
        {
            _funcionarioAppService = funcionarioAppService;
        }

        public ActionResult Index()
        {
            return View(_funcionarioAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var funcionarioViewModel = _funcionarioAppService.ObterPorId(id);
            if (funcionarioViewModel != null) return View(funcionarioViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FuncionarioViewModel funcionarioViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _funcionarioAppService.Adicionar(funcionarioViewModel);
                    return RedirectToAction("Index");
                }
                return View(funcionarioViewModel);
            }
            catch
            {
                return View(funcionarioViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var funcionarioViewModel = _funcionarioAppService.ObterPorId(id);
            if (funcionarioViewModel != null) return View(funcionarioViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(FuncionarioViewModel funcionarioViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _funcionarioAppService.Adicionar(funcionarioViewModel);
                    return RedirectToAction("Index");
                }
                return View(funcionarioViewModel);
            }
            catch
            {
                return View(funcionarioViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var funcionarioViewModel = _funcionarioAppService.ObterPorId(id);
            if (funcionarioViewModel != null) return View(funcionarioViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(FuncionarioViewModel funcionarioViewModel)
        {
            try
            {
                _funcionarioAppService.Remover(funcionarioViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(funcionarioViewModel);
            }
        }
    }
}
