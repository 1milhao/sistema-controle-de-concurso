﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class InscricaoService : ServiceBase<Inscricao>, IInscricaoService
    {
        private readonly IInscricaoRepository _repository;
        public InscricaoService(IInscricaoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}