﻿using System;
using Project.Domain.Entities.Enum;

namespace Project.Application.ViewModels
{
    public class InscricaoViewModel
    {
        public Guid Id { get; set; }
        public bool Ativa { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Status { get; set; }
        public string TipoInscricao { get; set; }
        public Tipo Tipo { get; set; }
        public virtual VagasViewModel Vagas { get; set; }
        public virtual BoletoViewModel Boleto { get; set; }
    }
}