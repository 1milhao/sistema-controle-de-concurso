﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Project.Domain.Entities;
using Project.Infra.Data.EntityConfig;

namespace Project.Infra.Data.Context
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("ProjectContext") { }

        public DbSet<AtosDaComissao> AtosDaComissao { get; set; }
        public DbSet<Boleto> Boleto { get; set; }
        public DbSet<Candidato> Candidato { get; set; }
        public DbSet<Comissao> Comissao { get; set; }
        public DbSet<Concurso> Concurso { get; set; }
        public DbSet<Funcionario> Funcionario { get; set; }
        public DbSet<Inscricao> Inscricao { get; set; }
        public DbSet<Vagas> Vagas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(150));

            modelBuilder.Configurations.Add(new AtosDaComissaoConfig());
            modelBuilder.Configurations.Add(new BoletoConfig());
            modelBuilder.Configurations.Add(new CandidatoConfig());
            modelBuilder.Configurations.Add(new CargoConfig());
            modelBuilder.Configurations.Add(new ComissaoConfig());
            modelBuilder.Configurations.Add(new ConcursoConfig());
            modelBuilder.Configurations.Add(new FuncionarioConfig());
            modelBuilder.Configurations.Add(new InscricaoConfig());
            modelBuilder.Configurations.Add(new InstituicaoConfig());
            modelBuilder.Configurations.Add(new VagasConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}