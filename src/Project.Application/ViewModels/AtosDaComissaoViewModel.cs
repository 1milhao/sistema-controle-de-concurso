﻿using System;

namespace Project.Application.ViewModels
{
    public class AtosDaComissaoViewModel
    {
        public Guid Id { get; set; }
        public int DataFimDaVisibilidade { get; set; }
        public DateTime DataInicioDaVisibilidade { get; set; }
        public string Tipo { get; set; }
        public byte[] Arquivo { get; set; }
        public bool Ativo { get; set; }
        public virtual ConcursoViewModel Concurso { get; set; }
    }
}