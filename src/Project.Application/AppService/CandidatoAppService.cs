﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class CandidatoAppService : ICandidatoAppService
    {
        private readonly IMapper _mapper;
        private readonly ICandidatoService _service;

        public CandidatoAppService(IMapper mapper, ICandidatoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<CandidatoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<CandidatoViewModel>>(_service.ObterTodos());
        }

        public CandidatoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<CandidatoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(CandidatoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Candidato>(obj));
        }

        public void Atualizar(CandidatoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Candidato>(obj));
        }

        public void Remover(CandidatoViewModel obj)
        {
            _service.Remover(_mapper.Map<Candidato>(obj));
        }
    }
}