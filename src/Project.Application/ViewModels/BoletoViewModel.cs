﻿using System;

namespace Project.Application.ViewModels
{
    public class BoletoViewModel
    {
        public Guid Id { get; set; }
        public byte[] Arquivo { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataDeValidade { get; set; }
    }
}