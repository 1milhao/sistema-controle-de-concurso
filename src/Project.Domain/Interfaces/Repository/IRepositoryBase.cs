﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Project.Domain.Interfaces.Repository
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : class
    {
        IEnumerable<TEntity> Procurar(Expression<Func<TEntity, bool>> predicate);
        TEntity ObterPorId(Guid id);
        IEnumerable<TEntity> ObterTodos();
        void Atualizar(TEntity obj);
        void Remover(TEntity obj);
        void Adicionar(TEntity obj);

    }
}