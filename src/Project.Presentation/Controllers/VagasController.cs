﻿using Project.Application.Interface;
using Project.Application.ViewModels;
using System;
using System.Web.Mvc;

namespace Project.Presentation.Controllers
{
    public class VagasController : Controller
    {
        private readonly IVagasAppService _vagasAppService;

        public VagasController(IVagasAppService vagasAppService)
        {
            _vagasAppService = vagasAppService;
        }

        public ActionResult Index()
        {
            return View(_vagasAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var vagasViewModel = _vagasAppService.ObterPorId(id);
            if (vagasViewModel != null) return View(vagasViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(VagasViewModel vagasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _vagasAppService.Adicionar(vagasViewModel);
                    return RedirectToAction("Index");
                }
                return View(vagasViewModel);
            }
            catch
            {
                return View(vagasViewModel);
            }
        }

        public ActionResult Edit(Guid id)
        {
            var vagasViewModel = _vagasAppService.ObterPorId(id);
            if (vagasViewModel != null) return View(vagasViewModel);
            return HttpNotFound();
        }

        public ActionResult Edit(VagasViewModel vagasViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _vagasAppService.Atualizar(vagasViewModel);
                    return RedirectToAction("Index");
                }
                return View(vagasViewModel);
            }
            catch
            {
                return View(vagasViewModel);
            }
        }

        public ActionResult Delete(Guid id)
        {
            var vagasViewModel = _vagasAppService.ObterPorId(id);
            if (vagasViewModel != null) return View(vagasViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Delete(VagasViewModel vagasViewModel)
        {
            try
            {
                _vagasAppService.Remover(vagasViewModel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(vagasViewModel);
            }
        }
    }
}