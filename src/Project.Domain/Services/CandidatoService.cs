﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class CandidatoService : ServiceBase<Candidato>, ICandidatoService
    {
        private readonly ICandidatoRepository _repository;
        public CandidatoService(ICandidatoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}