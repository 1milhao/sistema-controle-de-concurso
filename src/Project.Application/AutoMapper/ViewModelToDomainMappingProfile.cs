﻿using AutoMapper;
using Project.Application.ViewModels;
using Project.Domain.Entities;

namespace Project.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<AtosDaComissaoViewModel, AtosDaComissao>();
            CreateMap<BoletoViewModel, Boleto>();
            CreateMap<CandidatoViewModel, Candidato>();
            CreateMap<CargoViewModel, Cargo>();
            CreateMap<ComissaoViewModel, Comissao>();
            CreateMap<ConcursoViewModel, Concurso>();
            CreateMap<FuncionarioViewModel, Funcionario>();
            CreateMap<InscricaoViewModel, Inscricao>();
            CreateMap<InstituicaoViewModel, Instituicao>();
            CreateMap<VagasViewModel, Vagas>();
        }
    }
}