﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class FuncionarioAppService : IFuncionarioAppService
    {
        private readonly IMapper _mapper;
        private readonly IFuncionarioService _service;

        public FuncionarioAppService(IMapper mapper, IFuncionarioService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<FuncionarioViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<FuncionarioViewModel>>(_service.ObterTodos());
        }

        public FuncionarioViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<FuncionarioViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(FuncionarioViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Funcionario>(obj));
        }

        public void Atualizar(FuncionarioViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Funcionario>(obj));
        }

        public void Remover(FuncionarioViewModel obj)
        {
            _service.Remover(_mapper.Map<Funcionario>(obj));
        }
    }
}