﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class ComissaoService : ServiceBase<Comissao>, IComissaoService
    {
        private readonly IComissaoRepository _repository;
        public ComissaoService(IComissaoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}