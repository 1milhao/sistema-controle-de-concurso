﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IVagasAppService
    {
        IEnumerable<VagasViewModel> ObterTodos();
        VagasViewModel ObterPorId(Guid id);
        void Adicionar(VagasViewModel obj);
        void Atualizar(VagasViewModel obj);
        void Remover(VagasViewModel obj);
    }
}