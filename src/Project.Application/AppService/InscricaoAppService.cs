﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class InscricaoAppService : IInscricaoAppService
    {
        private readonly IMapper _mapper;
        private readonly IInscricaoService _service;

        public InscricaoAppService(IMapper mapper, IInscricaoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<InscricaoViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<InscricaoViewModel>>(_service.ObterTodos());
        }

        public InscricaoViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<InscricaoViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(InscricaoViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Inscricao>(obj));
        }

        public void Atualizar(InscricaoViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Inscricao>(obj));
        }

        public void Remover(InscricaoViewModel obj)
        {
            _service.Remover(_mapper.Map<Inscricao>(obj));
        }
    }
}