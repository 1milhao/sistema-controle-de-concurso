﻿using System;

namespace Project.Domain.Entities
{
    public class Boleto
    {
        public Boleto(Guid id, byte[] arquivo, bool ativo, DateTime dataDeValidade)
        {
            Id = id;
            Arquivo = arquivo;
            Ativo = ativo;
            DataDeValidade = dataDeValidade;
        }

        protected Boleto()
        {
            
        }

        public Guid Id { get; private set; }
        public byte[] Arquivo { get; private set; }
        public bool Ativo { get; private set; }
        public DateTime DataDeValidade { get; private set; }
    }
}