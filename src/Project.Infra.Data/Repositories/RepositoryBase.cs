﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Project.Domain.Interfaces.Repository;
using Project.Infra.Data.Context;
using Project.Infra.Data.Interfaces;

namespace Project.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly ProjectContext Context;
        protected DbSet<TEntity> DbSet;
        private readonly IUnitOfWork _uow;

        public RepositoryBase(ProjectContext context, IUnitOfWork uow)
        {
            Context = context;
            _uow = uow;
            DbSet = Context.Set<TEntity>();
        }

        protected void Commit()
        {
            _uow.SaveChanges();
        }

        public void Adicionar(TEntity obj)
        {
            DbSet.Add(obj);
            Commit();
        }

        public IEnumerable<TEntity> Procurar(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate).AsNoTracking();
        }

        public TEntity ObterPorId(Guid id)
        {
            return DbSet.Find(id);
        }

        public IEnumerable<TEntity> ObterTodos()
        {
            return DbSet.AsNoTracking().ToList();
        }

        public void Atualizar(TEntity obj)
        {
            var entry = Context.Entry(obj);
            DbSet.Attach(obj);
            entry.State = EntityState.Modified;
            Commit();
        }

        public void Remover(TEntity obj)
        {
            var entry = Context.Entry(obj);
            DbSet.Attach(obj);
            entry.State = EntityState.Deleted;
            Commit();
        }

        public void Dispose()
        {
            Context.Dispose();
            ;
        }
    }
}