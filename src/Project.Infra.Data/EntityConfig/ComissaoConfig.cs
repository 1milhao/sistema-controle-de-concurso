﻿using System.Data.Entity.ModelConfiguration;
using Project.Domain.Entities;

namespace Project.Infra.Data.EntityConfig
{
    public class ComissaoConfig : EntityTypeConfiguration<Comissao>
    {
        public ComissaoConfig()
        {
            //PK
            HasKey(c => c.Id);

            //PROPERTIES

            //DATA
            ToTable("Comissao");
            Property(c => c.Id).HasColumnName("Id");

            //RELATIONS
        }
    }
}