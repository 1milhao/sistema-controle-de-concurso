﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Project.Application.Interface;
using Project.Application.ViewModels;
using Project.Domain.Entities;
using Project.Domain.Interfaces.Service;

namespace Project.Application.AppService
{
    public class VagasAppService : IVagasAppService
    {
        private readonly IMapper _mapper;
        private readonly IVagasService _service;

        public VagasAppService(IMapper mapper, IVagasService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public IEnumerable<VagasViewModel> ObterTodos()
        {
            return _mapper.Map<IEnumerable<VagasViewModel>>(_service.ObterTodos());
        }

        public VagasViewModel ObterPorId(Guid id)
        {
            return _mapper.Map<VagasViewModel>(_service.ObterPorId(id));
        }

        public void Adicionar(VagasViewModel obj)
        {
            _service.Adicionar(_mapper.Map<Vagas>(obj));
        }

        public void Atualizar(VagasViewModel obj)
        {
            _service.Atualizar(_mapper.Map<Vagas>(obj));
        }

        public void Remover(VagasViewModel obj)
        {
            _service.Remover(_mapper.Map<Vagas>(obj));
        }
    }
}