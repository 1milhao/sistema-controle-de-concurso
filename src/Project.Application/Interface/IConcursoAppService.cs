﻿using System;
using System.Collections.Generic;
using Project.Application.ViewModels;

namespace Project.Application.Interface
{
    public interface IConcursoAppService
    {
        IEnumerable<ConcursoViewModel> ObterTodos();
        ConcursoViewModel ObterPorId(Guid id);
        void Adicionar(ConcursoViewModel obj);
        void Atualizar(ConcursoViewModel obj);
        void Remover(ConcursoViewModel obj);
    }
}