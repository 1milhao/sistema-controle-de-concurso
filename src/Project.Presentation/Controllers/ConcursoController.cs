﻿using System;
using System.Web.Mvc;
using Project.Application.Interface;
using Project.Application.ViewModels;

namespace Project.Presentation.Controllers
{
    public class ConcursoController : Controller
    {
        private readonly IConcursoAppService _consursoAppService;

        public ConcursoController(IConcursoAppService consursoAppService)
        {
            _consursoAppService = consursoAppService;
        }

        public ActionResult Index()
        {
            return View(_consursoAppService.ObterTodos());
        }

        public ActionResult Details(Guid id)
        {
            var concursoViewModel = _consursoAppService.ObterPorId(id);
            if (concursoViewModel != null) return View(concursoViewModel);
            return HttpNotFound();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ConcursoViewModel concursoViewModel)
        {
            if (ModelState.IsValid)
            {
                _consursoAppService.Adicionar(concursoViewModel);
                return RedirectToAction("Index");
            }

            return View(concursoViewModel);
        }

        public ActionResult Edit(Guid id)
        {
            var concursoViewModel = _consursoAppService.ObterPorId(id);
            if (concursoViewModel != null) return View(concursoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ConcursoViewModel concursoViewModel)
        {
            if (ModelState.IsValid)
            {
                _consursoAppService.Atualizar(concursoViewModel);
                return RedirectToAction("Index");
            }

            return View(concursoViewModel);
        }

        public ActionResult Delete(Guid id)
        {
            var concursoViewModel = _consursoAppService.ObterPorId(id);
            if (concursoViewModel != null) return View(concursoViewModel);
            return HttpNotFound();
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmDelete(ConcursoViewModel concursoViewModel)
        {
            _consursoAppService.Remover(concursoViewModel);
            return RedirectToAction("Index");
        }
    }
}