﻿using Project.Domain.Entities;
using Project.Domain.Interfaces.Repository;
using Project.Domain.Interfaces.Service;

namespace Project.Domain.Services
{
    public class ConcursoService : ServiceBase<Concurso>, IConcursoService
    {
        private readonly IConcursoRepository _repository;
        public ConcursoService(IConcursoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}